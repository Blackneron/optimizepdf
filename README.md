# OptimizePDF Script - README #
---

### Overview ###

The **OptimizePDF** script (Bash) uses the [**Ghostscript Interpreter (for PostScript language and PDF)**](https://www.ghostscript.com) to optimize **PDF** files in a specified folder and sends you an email notification if the process has finished. The script will also display information about the optimization progress and the size difference of each PDF file due to the compression and/or optimization. The script can be customized in the configuration section.

### Screenshots ###

![OptimizePDF - Optimization 1](development/readme/optimizepdf_1.png "OptimizePDF - Optimization 1")
![OptimizePDF - Optimization 2](development/readme/optimizepdf_2.png "OptimizePDF - Optimization 2")

### Setup ###

* Install the software packages **sendemail**, **libnet-ssleay-perl** and **libio-socket-ssl-perl**.
* You can use the command: **apt-get install sendemail libnet-ssleay-perl libio-socket-ssl-perl**
* Copy the script folder **optimizepdf** to your computer.
* Make the optimizer script **optimizepdf.sh** executable: **chmod +x optimizepdf.sh**.
* Edit the configuration section of the **optimizepdf.sh** script to fit your needs.
* Open the firewall port **587** if you want to send notification emails!
* Copy some PDF files into the **input** subfolder.
* Start the optimizer script from the command prompt: **./optimizepdf.sh**
* Optional you can specify a color resolution (in dpi) as a parameter: **./optimizepdf.sh 120**
* Get the optimized PDF files from the **output** subfolder and check if a notification was sent by email.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### Licenses ###

The **OptimizePDF** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT). The [**Ghostscript Interpreter**](https://www.ghostscript.com) is licensed under the [**GNU Affero General Public License (GNU AGPL)**](https://www.gnu.org/licenses/agpl-3.0.en.html). More information from Artifex about their licensing is available on the [**Artifex Website**](https://artifex.com/licensing/).
