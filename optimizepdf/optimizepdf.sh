#!/bin/bash


########################################################################
#                                                                      #
#       G H O S T S C R I P T   P D F   O P T I M I Z A T I O N        #
#                                                                      #
#               SCRIPT TO OPTIMIZE PDF FILES IN A FOLDER               #
#                                                                      #
#                      Copyright 2018 by PB-Soft                       #
#                                                                      #
#                           www.pb-soft.com                            #
#                                                                      #
#                      Version 1.10 / 22.05.2018                       #
#                                                                      #
########################################################################
#
#  1 - Check that the free 'ghostscript' software package is installed
#      on your system (https://ghostscript.com). Please check that the
#      installed version is not an old one: Version >= 9.23
#
#  2 - Customize the configuration section of this script and specify
#      the paths (if necessary) and the optimization settings.
#
#  3 - Copy some PDF files into the subfolder 'input'.
#
#  4 - Run this script to compress them and get the optimized files
#      from the subfolder 'output'.
#
# ======================================================================
#
# Check that the following software packages are installed on your
# system if you want to send emails:
#
#  - sendEmail (lightweight, command line SMTP email client)
#
#  - libnet-ssleay-perl (perl module to call Secure Sockets Layer (SSL)
#                       functions of the SSLeay library)
#
#  - libio-socket-ssl-perl (perl module that uses SSL to encrypt data)
#
# ======================================================================
#
# You can use the following commands to install the packages:
#
#   apt-get install sendemail
#
#   apt-get install libnet-ssleay-perl
#
#   apt-get install libio-socket-ssl-perl
#
# ======================================================================
#
# Check the following link for information about the sendEmail tool:
#
#   http://caspian.dotconf.net/menu/Software/SendEmail/
#
# ======================================================================
#
# Check that the firewall port 587 for SMTP (Simple Mail Transfer Pro-
# tocol) is open if you want to send notification emails!
#
# ======================================================================


########################################################################
########################################################################
##############   C O N F I G U R A T I O N   B E G I N   ###############
########################################################################
########################################################################


# ======================================================================
# Specify the OptimizePDF version.
# ======================================================================
OPTIMIZEPDF_VERSION=1.10


# ======================================================================
# Specify the path of the actual directory.
# ======================================================================
ACTUAL_DIRECTORY="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"


# ======================================================================
# Specify the input/output paths.
# ======================================================================
INPUT_PATH=$ACTUAL_DIRECTORY/input
OUTPUT_PATH=$ACTUAL_DIRECTORY/output


# ======================================================================
# Specify the path to the ghostscript binary.
# ======================================================================
GHOSTSCRIPT_PATH=$ACTUAL_DIRECTORY/gs


# ======================================================================
# Specify the path to the logfile.
# ======================================================================
LOGFILE_PATH=$ACTUAL_DIRECTORY/optimizepdf.log


# ======================================================================
# Specify the default image resolution (image quality) in dpi.
# ======================================================================
DEFAULT_IMAGE_RESOLUTION=120


# ======================================================================
# Specify the PDF compatibility version.
# ======================================================================
PDF_VERSION=1.7


# ======================================================================
# Specify the number of CPU cores for processing the PDF files.
#
# Check the following link for more information about the ghostscript
# performance settings:
#
# https://ghostscript.com/doc/current/Use.htm#Improving_performance
#
# ======================================================================
CPU_CORES=2


# ======================================================================
# Specify the additional memory (in MB) provided for the optimization.
#
# Check the following link for more information about the ghostscript
# performance settings:
#
# https://ghostscript.com/doc/current/Use.htm#Improving_performance
#
# ======================================================================
EXTRA_MEMORY_MB=30


# ======================================================================
# Specify if email notifications should be sent (yes/no).
# ======================================================================
EMAIL_NOTIFICATIONS="no"


# ======================================================================
# Specify the connection details for the SMTP host.
# ======================================================================
EMAIL_USERNAME="john.doe@gmail.com"
EMAIL_PASSWORD="smtp_password"
EMAIL_HOSTNAME="smtp.gmail.com"
EMAIL_PORT="587"
EMAIL_TLS="yes"


# ======================================================================
# Specify the email sender.
# ======================================================================
EMAIL_SENDER="admin@example.com"


# ======================================================================
# Specify the email receiver(s). Multiple receivers may be specified by
# separating them by either a white space, comma, or semi-colon like:
#
#  EMAIL_RECEIVER="john@example.com brad@domain.com"
#
# ======================================================================
EMAIL_RECEIVER="john.doe@yahoo.com"


########################################################################
########################################################################
################   C O N F I G U R A T I O N   E N D   #################
########################################################################
########################################################################


# ======================================================================
# Clear the screen.
# ======================================================================
clear


# ======================================================================
# Specify the script start time.
# ======================================================================
SCRIPT_START=`date +%s`


# ======================================================================
# Calculate the extra memory in bytes.
# (Ghostscript uses the calculation: 1 MB = 1000000 B).
# ======================================================================
EXTRA_MEMORY_B=$(($EXTRA_MEMORY_MB*1000000))


# ======================================================================
# Initialize the file counter.
# ======================================================================
FILE_COUNTER=0


# ======================================================================
# Initialize the copy counter.
# ======================================================================
COPY_COUNTER=0


# ======================================================================
# Initialize the optimized counter.
# ======================================================================
OPTIMIZED_COUNTER=0


# ======================================================================
# Get the paths of the 'sendEmail' executable.
# ======================================================================
SENDEMAIL="$(which sendEmail)"


# ======================================================================
# Get the image resolution value from the command line.
# ======================================================================
IMAGE_RESOLUTION=$1


# ======================================================================
# Get the version of the installed ghostscript binary.
# ======================================================================
GHOSTSCRIPT_VERSION=$($GHOSTSCRIPT_PATH -v | grep -i "Ghostscript"| awk '{print $3}')


# ======================================================================
# Check if the image resolution was not specified by the user.
# ($# holds the number of arguments passed to the script)
# ======================================================================
if [ $# -eq 0 ]; then


  # ====================================================================
  # Use the default image resolution value.
  # ====================================================================
  IMAGE_RESOLUTION=$DEFAULT_IMAGE_RESOLUTION
fi


########################################################################
########################################################################
###############   C R E A T E   D I R E C T O R I E S   ################
########################################################################
########################################################################


# ======================================================================
# Check if the input directory does not exist.
# ======================================================================
if [ ! -d $INPUT_PATH ]; then


  # ====================================================================
  # Create the input directory.
  # ====================================================================
  mkdir -p $INPUT_PATH
fi


# ======================================================================
# Check if the output directory does not exist.
# ======================================================================
if [ ! -d $OUTPUT_PATH ]; then


  # ====================================================================
  # Create the output directory.
  # ====================================================================
  mkdir -p $OUTPUT_PATH
fi


########################################################################
########################################################################
#############   D E L E T E   O L D   P D F   F I L E S   ##############
########################################################################
########################################################################


# ======================================================================
# Check if the output directory is not empty.
# ======================================================================
if [ "$(ls -A $OUTPUT_PATH)" ]; then


  # ====================================================================
  # Remove the PDF files from the output directory.
  # ====================================================================
  find $OUTPUT_PATH -maxdepth 1 -type f -iname "*.pdf" -exec rm {} +
fi


########################################################################
########################################################################
###############   D E L E T E   O L D   L O G F I L E   ################
########################################################################
########################################################################


# ======================================================================
# Check if an old logfile exist.
# ======================================================================
if [ -f $LOGFILE_PATH ]; then


  # ====================================================================
  # Remove the old logfile.
  # ====================================================================
  rm $LOGFILE_PATH
fi


########################################################################
########################################################################
#################   P D F   O P T I M I Z A T I O N   ##################
########################################################################
########################################################################


# ======================================================================
# Display the start bar.
# ======================================================================
echo "================================================================================"
echo
echo "                        P D F   O P T I M I Z A T I O N"
echo
echo "                           OptimizePDF version: $OPTIMIZEPDF_VERSION"
echo "                           Ghostscript version: $GHOSTSCRIPT_VERSION"
echo
echo "================================================================================"
echo


# ======================================================================
# Find all PDF files in the input folder.
# ======================================================================
find $INPUT_PATH -iname "*.pdf" |
{


  # ====================================================================
  # Loop through the input folder to process all PDF files. The whole
  # processing operation will run in the subprocess because otherwise
  # the 'Total' variables are empty. Check the following website for
  # more information about this behavior:
  #
  #    http://mywiki.wooledge.org/BashFAQ/024
  #
  # ====================================================================
  while read f; do


    # ==================================================================
    # Specify the PDF optimization start time.
    # ==================================================================
    OPT_START=`date +%s`


    # ==================================================================
    # Increase the file counter.
    # ==================================================================
    FILE_COUNTER=$((FILE_COUNTER + 1))


    # ==================================================================
    # Get the actual filename.
    # ==================================================================
    INPUT_FILENAME=$(basename $f)


    # ==================================================================
    # Get the actual filename without the extension.
    # ==================================================================
    FILENAME_ONLY="${INPUT_FILENAME%.*}"


    # ==================================================================
    # Get the actual file extension.
    # ==================================================================
    EXTENSION="${INPUT_FILENAME##*.}"


    # ==================================================================
    # Add the 'pdf' extension to the output file name.
    # ==================================================================
    OUTPUT_FILENAME=$FILENAME_ONLY.$EXTENSION


    # ==================================================================
    # Display the name of the actual processed PDF file.
    # ==================================================================
    echo " $FILE_COUNTER | Optimizing '$INPUT_FILENAME' with a color resolution of $IMAGE_RESOLUTION dpi..."


    # ==================================================================
    # Write the actual filename to the logfile.
    # ==================================================================
    echo "$FILE_COUNTER | Optimizing file: '$INPUT_FILENAME'" >> $LOGFILE_PATH 2>&1


    # ==================================================================
    # Optimize the actual PDF file.
    # ==================================================================
    $GHOSTSCRIPT_PATH -q \
        -dNOPAUSE \
        -dBATCH \
        -dSAFER \
        -dQUIET \
        -dINTERPOLATE \
        -dNumRenderingThreads=$CPU_CORES \
        -sDEVICE=pdfwrite \
        -dCompatibilityLevel=$PDF_VERSION \
        -dEmbedAllFonts=true \
        -dSubsetFonts=true \
        -dAutoRotatePages=/None \
        -dDetectDuplicateImages=true \
        -dDownsampleColorImages=true \
        -dColorImageDownsampleType=/Bicubic \
        -dColorImageResolution=$IMAGE_RESOLUTION \
        -dDownsampleGrayImages=true \
        -dGrayImageDownsampleType=/Bicubic \
        -dGrayImageResolution=300 \
        -dDownsampleMonoImages=true \
        -dMonoImageDownsampleType=/Subsample \
        -dMonoImageResolution=300 \
        -sOutputFile=$OUTPUT_PATH/$OUTPUT_FILENAME \
        -c "$EXTRA_MEMORY_B setvmthreshold" \
        -f $f >> $LOGFILE_PATH 2>&1


    # ==================================================================
    # Calculate the size of the actual input file.
    # ==================================================================
    FILESIZE_INPUT=$(stat -c%s "$f")


    # ==================================================================
    # Calculate the size of the actual output file.
    # ==================================================================
    FILESIZE_OUTPUT=$(stat -c%s "$OUTPUT_PATH/$OUTPUT_FILENAME")


    # ==================================================================
    # Specify the PDF optimization end time.
    # ==================================================================
    OPT_END=`date +%s`


    # ==================================================================
    # Calculate the PDF optimization processing time.
    # ==================================================================
    OPT_TIME=$((OPT_END-OPT_START))


    # ==================================================================
    # Check if the actual PDF file could not be optimized.
    # ==================================================================
    if [ "$FILESIZE_OUTPUT" -ge "$FILESIZE_INPUT" ]; then


      # ================================================================
      # Calculate the total input size.
      # ================================================================
      TOTAL_FILESIZE_INPUT=$((TOTAL_FILESIZE_INPUT+FILESIZE_INPUT))


      # ================================================================
      # Calculate the total output size.
      # ================================================================
      TOTAL_FILESIZE_OUTPUT=$((TOTAL_FILESIZE_OUTPUT+FILESIZE_INPUT))


      # ================================================================
      # Calculate the size increase in KB.
      # ================================================================
      SIZE_INCREASE_KB=$(((FILESIZE_OUTPUT-FILESIZE_INPUT)/1024))


      # ================================================================
      # Calculate the size increase in percent.
      # ================================================================
      SIZE_INCREASE_PERCENT=$(bc <<< "scale=2; ($FILESIZE_OUTPUT-$FILESIZE_INPUT)*10000/$FILESIZE_INPUT/100")


      # ================================================================
      # Display an information message.
      # ================================================================
      echo "     The PDF could not be optimized and was copied to the output directory!"
      echo "     --> Filesize input:  $FILESIZE_INPUT"
      echo "     --> Filesize output: $FILESIZE_OUTPUT (Increase: ${SIZE_INCREASE_KB}KB / ${SIZE_INCREASE_PERCENT}%)"
      echo


      # ================================================================
      # Copy the original file to the output directory.
      # ================================================================
      cp $f $OUTPUT_PATH/$OUTPUT_FILENAME


      # ================================================================
      # Increase the copy counter.
      # ================================================================
      COPY_COUNTER=$((COPY_COUNTER + 1))


      # ================================================================
      # The actual PDF file could be optimized.
      # ================================================================
    else


      # ================================================================
      # Calculate the total input size.
      # ================================================================
      TOTAL_FILESIZE_INPUT=$((TOTAL_FILESIZE_INPUT+FILESIZE_INPUT))


      # ================================================================
      # Calculate the total output size.
      # ================================================================
      TOTAL_FILESIZE_OUTPUT=$((TOTAL_FILESIZE_OUTPUT+FILESIZE_OUTPUT))


      # ================================================================
      # Calculate the size difference of the actual file - Percent.
      # ================================================================
      DIFFERENCE_PERCENT=$(bc <<< "scale=2; ($FILESIZE_INPUT-$FILESIZE_OUTPUT)*10000/$FILESIZE_INPUT/-100")


      # ================================================================
      # Calculate the size difference of the actual file - KB.
      # ================================================================
      DIFFERENCE_KB=$(bc <<< "scale=2; ($FILESIZE_INPUT-$FILESIZE_OUTPUT)/-1024")


      # ================================================================
      # Calculate the size difference of the actual file - MB.
      # ================================================================
      DIFFERENCE_MB=$(bc <<< "scale=2; ($FILESIZE_INPUT-$FILESIZE_OUTPUT)/-(1024*1024)")


      # ================================================================
      # Increase the optimized counter.
      # ================================================================
      OPTIMIZED_COUNTER=$((OPTIMIZED_COUNTER + 1))


      # ================================================================
      # Display the size difference of the actual file.
      # ================================================================
      echo "     Size difference: $DIFFERENCE_PERCENT% or ${DIFFERENCE_KB}KB / ${DIFFERENCE_MB}MB (in $OPT_TIME seconds)"
      echo
    fi
  done


  ######################################################################
  ######################################################################
  ###################   S H O W   O V E R V I E W   ####################
  ######################################################################
  ######################################################################


  # ====================================================================
  # Calculate the size difference of the actual file - Percent.
  # ====================================================================
  TOTAL_DIFFERENCE_PERCENT=$(bc <<< "scale=2; ($TOTAL_FILESIZE_INPUT-$TOTAL_FILESIZE_OUTPUT)*10000/$TOTAL_FILESIZE_INPUT/-100")


  # ====================================================================
  # Calculate the size difference of the actual file - KB.
  # ====================================================================
  TOTAL_DIFFERENCE_KB=$(bc <<< "scale=2; ($TOTAL_FILESIZE_INPUT-$TOTAL_FILESIZE_OUTPUT)/-1024")


  # ====================================================================
  # Calculate the size difference of the actual file - MB.
  # ====================================================================
  TOTAL_DIFFERENCE_MB=$(bc <<< "scale=2; ($TOTAL_FILESIZE_INPUT-$TOTAL_FILESIZE_OUTPUT)/-(1024*1024)")


  # ====================================================================
  # Display the number of optimized files.
  # ====================================================================
  echo "================================================================================"
  echo
  echo " Total optimized files: $OPTIMIZED_COUNTER"
  echo " Total copied files:    $COPY_COUNTER"
  echo " ---------------------------"
  echo " Total processed files: $FILE_COUNTER"
  echo


  # ====================================================================
  # Display the size difference of the actual file.
  # ====================================================================
  echo " Total size difference: $TOTAL_DIFFERENCE_PERCENT% or ${TOTAL_DIFFERENCE_KB}KB / ${TOTAL_DIFFERENCE_MB}MB"
  echo


  # ====================================================================
  # Specify the script end time.
  # ====================================================================
  SCRIPT_END=`date +%s`


  # ====================================================================
  # Calculate the script runtime.
  # ====================================================================
  SCRIPT_TIME=$((SCRIPT_END-SCRIPT_START))


  # ====================================================================
  # Display the script runtime.
  # ====================================================================
  echo " Total processing time: $SCRIPT_TIME seconds"
  echo
  echo "================================================================================"
  echo


  ######################################################################
  ######################################################################
  #############   E - M A I L   N O T I F I C A T I O N   ##############
  ######################################################################
  ######################################################################


  # ====================================================================
  # Check if email notifications should be sent.
  # ====================================================================
  if [ "$EMAIL_NOTIFICATIONS" == "yes" ]; then


    # ==================================================================
    # Specify the actual date in the 'dd.mm.yyyy' format.
    # ==================================================================
    EMAIL_DATE="$(date +"%d.%m.%Y")"


    # ==================================================================
    # Specify the actual time in the 'hh:mm:ss' format.
    # ==================================================================
    EMAIL_TIME="$(date +"%H:%M:%S")"


    # ==================================================================
    # Get the actual hostname where the script is running.
    # ==================================================================
    SCRIPT_HOST="$(hostname)"


    # ==================================================================
    # Specify more email details.
    # ==================================================================
    EMAIL_SUBJECT="$EMAIL_DATE / $EMAIL_TIME | PDF optimization of $FILE_COUNTER files terminated!"
    EMAIL_MESSAGE="Start date: $EMAIL_DATE\nStart time: $EMAIL_TIME\nHostname: $SCRIPT_HOST\nNumber of files: $FILE_COUNTER\nProcessing time: $SCRIPT_TIME s\nSize difference in %: $TOTAL_DIFFERENCE_PERCENT\nSize difference in KB: $TOTAL_DIFFERENCE_KB\nSize difference in MB: $TOTAL_DIFFERENCE_MB"


    # ==================================================================
    # Send a notification via email.
    # ==================================================================
    $SENDEMAIL -o tls=$EMAIL_TLS -f $EMAIL_SENDER -t $EMAIL_RECEIVER -s $EMAIL_HOSTNAME:$EMAIL_PORT -xu $EMAIL_USERNAME -xp $EMAIL_PASSWORD -u "$EMAIL_SUBJECT" -m "$EMAIL_MESSAGE" -o message-charset=utf-8


    # ==================================================================
    # Display the end line.
    # ==================================================================
    echo
    echo "================================================================================"
    echo
  fi
}
